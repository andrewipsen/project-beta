# CarCar

Welcome to CarCar, An application designed for dealerships in need of a property management system to manage their inventory, sales, services, and employees.

The backend of the CarCar application is constructed using Django, RestAPI, and uses a poller to get data from another microservice, while the front end is made using React.js.

### CarCar Features:

- Create & List
  - Auto Manufacturers
  - Automobiles Models
  - Automobiles
  - Salespeople
    - Sales Person history page allows the user to see all sales made and includes functionality to filter results by sales person.
  - Customers
  - Sales
  - Technicians
  - Service Appointments
    - VIP feature checks the VIN # and automatically updates if the customer bought their car in the dealership
    - Service history page includes functionality to search service history by VIN

### Team:

- Andrew Ipsen - Sales Microservice
- Mo Rahman - Service Microservice

## To Run

1. Be sure you are in your desired directory in your terminal.
2. From within your terminal, you can clone the repository with `git clone https://gitlab.com/andrewipsen/project-beta.git`
3. Assuming you have Docker Desktop installed, run the following commands:

```
docker volume create beta-data
docker-compose build
docker-compose up

```

### (For macOS users, you can safely ignore the warning about a missing environment variable named OS)

4. At this point, the service should be running and can be accessed on the ports defined in your Docker Desktop Container window.
5. The development server can be found on `http://localhost:3000`. It may take several minutes for the React page to finish rendering.

## Design

![excalidraw](thedesign.PNG)

## Service microservice

The Service microservice allows users to manage service appointments and technicians.

Appointments can be created, listed, and their status can either be changed from created to canceled or finished.

When Appointments are listed, there is built in functionality to check whether the VIN # associated with the appointment was sold at the same dealership, if so, the customer will show a “VIP” status.

Technicians can be created, listed, and deleted. There is built in functionality to automatically generate an employee Id associated with the technician being created.

### Models

1. **AutomobileVO**
2. **Technician**
3. **Appointment**

The Appointment model uses the Technician model as a foreign key.

### Integration with the Inventory microservice:

In the Service microservice, data from the Inventory microservice regarding automobile VINs is polled to an AutomobileVO model. Once a “GET” request is processed, a function checks whether or not the VIN entered for the service appointment also exists in the AutomobileVO. If it does exist, the VIP status of that appointment is set to True.

### RESTful API calls

1. ### Appointments

   | Method | Action              | Url                                                |
   | ------ | ------------------- | -------------------------------------------------- |
   | GET    | List Appointments   | http://localhost:8080/api/appointments/            |
   | POST   | Create Appointments | http://localhost:8080/api/appointments/            |
   | DELETE | Delete Appointments | http://localhost:8080/api/appointments/:id         |
   | PUT    | Finish Appointments | http://localhost:8080/api/appointments/:id/finish/ |
   | PUT    | Cancel Appointments | http://localhost:8080/api/appointments/:id/cancel/ |

<details>
<summary markdown="span">GET: This will return a list of all service appointments. The following is the JSON data that's returned</summary>

JSON request body:

```
{
	"appointment": [
		{
			"vin": "1C3CC5FB2AN120174",
			"date_time": "2023-05-23T16:00:00+00:00",
			"technician": {
				"first_name": "Mo",
				"last_name": "Rahman",
				"employee_id": 1
			},
			"reason": "Oil Change",
			"status": "",
			"customer": "Maria Lopez",
			"id": 5,
			"vip": false
		},
	]
}

```

</details>

<details>
<summary markdown="span">POST: This will create an appointment. The following is the JSON data that's posted</summary>
JSON request body:

(note: The technican field represents the employee ID of the technician)

```
{
	"date_time": "2023-05-23 18:00",
	"reason": "Oil Change",
	"status": "created",
	"vin": "1C3CCBBG2EN115059",
	"customer": "John Smith",
	"technician": 5
}

```

</details>

<details>
<summary markdown="span">DELETE: This will delete the appointment from the database. To delete, The correct appointment ID must be in the URL. The follwing is the JSON data that appears upon deletion, and the request URL</summary>

URL: `http://localhost:8080/api/appointments/:id`

JSON request body:

```
{
	"deleted": true
}

```

</details>

<details>
<summary markdown="span">PUT: You can make a `PUT` request to change the status of an appointment to `canceled` or `finished`. In order to make the request, The correct appointment ID must be in the URL. The folling is the JSON data that is returned, and the URL.</summary>

Cancel URL: `http://localhost:8080/api/appointments/:id/cancel/`

JSON request body :

```
{
	"vin": "1C3CC5FB2AN120174",
	"date_time": "2023-05-23T16:00:00+00:00",
	"technician": {
		"first_name": "Mo",
		"last_name": "Rahman",
		"employee_id": 1
	},
	"reason": "Oil Change",
	"status": "canceled",
	"id": 4
}

```

Finish URL: `http://localhost:8080/api/appointments/:id/finish/`

JSON request body :

```
{
	"vin": "1C3CC5FB2AN120174",
	"date_time": "2023-05-23T16:00:00+00:00",
	"technician": {
		"first_name": "Mo",
		"last_name": "Rahman",
		"employee_id": 1
	},
	"reason": "Oil Change",
	"status": "finished",
	"id": 3
}

```

</details>

2. ### Technicians

   | Method | Action             | Url                                      |
   | ------ | ------------------ | ---------------------------------------- |
   | GET    | List Technicians   | http://localhost:8080/api/technicians/   |
   | POST   | Create Technicians | http://localhost:8080/api/technicians/   |
   | DELETE | Delete Technicians | http://localhost:8080/api/technicians/7/ |

<details>
<summary markdown="span">GET: This will return a list of all technicians. The following is the JSON data that's returned</summary>

JSON request body :

```
{
	"technician": [
		{
			"first_name": "John",
			"last_name": "Smith",
			"employee_id": 1
		},
	]
}

```

</details>

<details>
<summary markdown="span">POST: This will create a new technician. No employee ID is needed as it is automatically generated. The following is the JSON data that's posted</summary>

JSON request body :

```
{
  "first_name": "John",
  "last_name": "Smith"
}

```

</details>

<details>
<summary markdown="span">DELETE: This will delete the technician from the database. To delete, The correct employee ID must be in the URL. The follwing is the JSON data that appears upon deletion, and the request URL</summary>

URL: `http://localhost:8080/api/technicians/:id`

JSON request body:

```
{
	"deleted": true
}

```

</details>

## Sales microservice

The Sales functionality keeps track of automobile sales that come from the inventory. A person cannot sell a car that is not listed in the inventory, nor can a person sell a car that has already been sold. It lives on port 8090.

1. **Salesperson**
2. **Customer**
3. **AutomobileVO**
4. **Sale**

The `Salesperson` model contains the following fields: `first_name`, `last_name`, and `employee_id`.

The `Customer` model contains the following fields: `first_name`, `last_name`, `address`, and `phone_number`.

The `AutomobileVO` model conatians the field, `import_vin`, and is a value object which represents of the **Automobile model** in the **_Inventory Microservice_**. The functionality of the AutomobileVO model is reliant on a polling function, which grabs the VIN data from the **_Inventory Microservice's_** **Automobile model**, refreshing every 60 seconds.

The `Sale` model contains the following _Foreignkey_ fields: `Salesperson`, `Customer`, and `Automobile`, each corresponding to the respective models above. A `Price` field is also included.

### RESTful API calls

---

1. ### Salesperson
   | Method | Action             | Url                                       |
   | ------ | ------------------ | ----------------------------------------- |
   | GET    | List Salespeople   | http://localhost:8090/api/salespeople/    |
   | POST   | Create Salesperson | http://localhost:8090/api/salespeople/    |
   | DELETE | Delete Salesperson | http://localhost:8090/api/salespeople/:id |

<details>
<summary markdown="span">GET: The return value of the list of salespeople is a dictionary, and will look like this:.
</summary>
JSON request body:

```

{
"salespersons": [
{
"first_name": "billy",
"last_name": "bob",
"employee_id": 8
},
]
}

```

</details>

<details>
<summary markdown="span">POST: To create a salesperson, the shape of the data should look like this. The employee_id is an "AutoField", which is automatically generated upon creation of the instance, and therefore need not be included in the shape you submit.
</summary>
JSON request body:

```

{
"first_name": "sally",
"last_name": "soo"
}

```

</details>

<details>
<summary markdown="span">DELETE: Deleting a salesperson requires knowing the employee_id of said salesperson:
</summary>

```

http://localhost:8090/api/salespeople/:id

```

</details>

2. ### Customer
   | Method | Action          | Url                                     |
   | ------ | --------------- | --------------------------------------- |
   | GET    | List Customers  | http://localhost:8090/api/customers/    |
   | POST   | Create Customer | http://localhost:8090/api/customers/    |
   | DELETE | Delete Customer | http://localhost:8090/api/customers/:id |

<details>
<summary markdown="span">GET: The return value of the list of customers is a dictionary, and will look like this:.
</summary>
JSON request body:

```

{
"customers": [
{
"id": 3,
"first_name": "jill",
"last_name": "doe",
"address": "123 first drive",
"phone_number": 1234567890
},
]
}

```

</details>

<details>
<summary markdown="span">POST: To create a customer, the shape of the data should look like this.
</summary>
JSON request body:

```

{
"first_name": "kevin",
"last_name": "win",
"address": "456 2nd drive",
"phone_number": 1234567890
}

```

</details>

<details>
<summary markdown="span">DELETE: Deleting a salesperson requires knowing the id of said customer:
</summary>

```

http://localhost:8090/api/customers/:id

```

</details>

3. ### Sale
   | Method | Action      | Url                                 |
   | ------ | ----------- | ----------------------------------- |
   | GET    | List Sales  | http://localhost:8090/api/sales/    |
   | POST   | Create Sale | http://localhost:8090/api/sales/    |
   | DELETE | Delete Sale | http://localhost:8090/api/sales/:id |

<details>
<summary markdown="span">GET: The return value of the list of sales is a dictionary, and will look like this:.
</summary>
JSON request body:

```

{
"sales": [
{
"id": 8,
"price": 200000,
"salesperson": {
"first_name": "billy",
"last_name": "bob",
"employee_id": 8
},
"customer": {
"id": 3,
"first_name": "jill",
"last_name": "doe",
"address": "123 first drive",
"phone_number": 1234567890
},
"automobile": {
"import_vin": "1C3CC5FB2AN120175"
}
},
]
}

```

</details>

<details>
<summary markdown="span">POST: To create a sale, the shape of the data should look like this. The value of both salesperson and customer must reference pre-existing instances of their respective models. The value for automobile must be a pre-existing "vin" value from an automobile instance from the Inventory microservice.
</summary>
JSON request body:

```

{
{
"price": 15000,
"salesperson": 9,
"customer": 4,
"automobile": "1C3CC5FB2AN120ABC"
}
}

```

</details>

<details>
<summary markdown="span">DELETE: Deleting a sale requires knowing the id of said sale:
</summary>

```

http://localhost:8090/api/sales/:id

```

</details>

## Inventory microservice

The Inventory microservice allows users to manage vehicle models, manufacturers, and assign automobile attributes such as year, color, and VIN# to vehicle models created.

A manufacturer first needs to be created to assign a model to that manufacturer. Once created, users are able to create models and assign them to their manufacturer.

Once a model is created, users are able to create an automobile to assign year, color, and a VIN to any model created. Upon completion of the automobile, the vehicle is ready for sale!

### Models

1. **Manufacturer**
2. **VehicleModel**
3. **Automobile**

The VehicleModel model uses the Manufacturer model as a foreign key.
The Automobile model uses the VehcileModel model as a foreign key.

1. ### Manufacturer
   | Method | Action              | Url                                          |
   | ------ | ------------------- | -------------------------------------------- |
   | GET    | List Manufacturer   | http://localhost:8100/api/manufacturers/     |
   | POST   | Create Manufacturer | http://localhost:8100/api/manufacturers/     |
   | DELETE | Delete Manufacturer | http://localhost:8100/api/manufacturers/:id/ |
   | PUT    | Update Manufacturer | http://localhost:8100/api/manufacturers/:id/ |

<details>
<summary markdown="span">GET: This will return a list of all manufacturers. The following is the JSON data that's returned</summary>

JSON request body :

```
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Honda"
		}
	]
}

```

</details>

<details>
<summary markdown="span">POST: This will create a new manufacturer. The following is the JSON data that's needed for the request</summary>

JSON request body :

```
{
  "name": "Honda"
}

```

</details>

<details>
<summary markdown="span">DELETE: This will delete the manufacturer from the database. To delete, The correct manufacturer ID must be in the URL. The follwing is the request URL needed.</summary>

URL: `http://localhost:8100/api/manufacturers/:id`

</details>

<details>
<summary markdown="span">PUT: This will update the manufacturer in the database. To update, The correct manufacturer ID must be in the URL. The folling is the JSON data that is needed, and the URL.</summary>

URL: `http://localhost:8100/api/manufacturers/:id`

JSON request body :

```
{
  "name": "Honda"
}

```

</details>

2. ### Vehcile-Model
   | Method | Action        | Url                                          |
   | ------ | ------------- | -------------------------------------------- |
   | GET    | List Models   | http://localhost:8100/api/models/            |
   | POST   | Create Models | http://localhost:8100/api/models/            |
   | DELETE | Delete Models | http://localhost:8100/api/manufacturers/:id/ |
   | PUT    | Update Models | http://localhost:8100/api/manufacturers/:id/ |

<details>
<summary markdown="span">GET: This will return a list of all vehicle models. The following is the JSON data that's returned</summary>

JSON request body :

```
{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Civic",
			"picture_url": "https://alinktoyourphoto.com/HondaCivic.jpeg",
			"manufacturer": {
				"href": "/api/manufacturers/5/",
				"id": 5,
				"name": "Honda"
			}
		}
	]
}

```

</details>

<details>
<summary markdown="span">POST: This will create a new vehicle model. The following is the JSON data that's needed for the request</summary>

JSON request body :
(note: manufacturer_id should be the "id" value of manufacturer)

```

{
  "name": "Civic",
  "picture_url": "https://alinktoyourphoto.com/HondaCivic.jpeg",
  "manufacturer_id": 5
}

```

</details>

<details>
<summary markdown="span">DELETE: This will delete the vehicle model from the database. To delete, The correct model ID must be in the URL. The follwing is the request URL needed.</summary>

URL: `http://localhost:8100/api/models/:id`

</details>

<details>
<summary markdown="span">PUT: This will update the vehicle model in the database. To update, The correct model ID must be in the URL. The folling is the JSON data that is needed, and the URL.</summary>

URL: `http://localhost:8100/api/models/:id`

JSON request body :

```
{
  "name": "Civic",
  "picture_url": "https://alinktoyourphoto.com/HondaCivic.jpeg",
  "manufacturer_id": 5
}

```

</details>

3. ### Automobiles
   | Method | Action             | Url                                        |
   | ------ | ------------------ | ------------------------------------------ |
   | GET    | List Automobiles   | http://localhost:8100/api/automobiles/     |
   | POST   | Create Automobiles | http://localhost:8100/api/automobiles/     |
   | DELETE | Delete Automobiles | http://localhost:8100/api/automobiles/:id/ |
   | PUT    | Update Automobiles | http://localhost:8100/api/automobiles/:id/ |

<details>
<summary markdown="span">GET: This will return a list of all automobiles. The following is the JSON data that's returned</summary>

JSON request body :

```
{
	"autos": [
		{
			"href": "/api/automobiles/1C3CC5FB2AN120164/",
			"id": 1,
			"color": "red",
			"year": 2023,
			"vin": "1C3CC5FB2AN120164",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Civic",
				"picture_url": https://alinktoyourphoto.com/HondaCivic.jpeg,
				"manufacturer": {
					"href": "/api/manufacturers/5/",
					"id": 5,
					"name": "Honda"
				}
			},
			"sold": false
		}
	]
}

```

</details>

<details>
<summary markdown="span">POST: This will create a new automobile. The following is the JSON data that's needed for the request</summary>

JSON request body :
(note: model_id should be the "id" value of model)

```

{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120164",
  "model_id": 1
}

```

</details>

<details>
<summary markdown="span">DELETE: This will delete the automobile from the database. To delete, The correct automobile ID must be in the URL. The follwing is the request URL needed.</summary>

URL: `http://localhost:8100/api/automobiles/:id`

</details>

<details>
<summary markdown="span">PUT: This will update the automobile in the database. To update, The automobile ID must be in the URL. The folling is the JSON data that is needed, and the URL.</summary>

URL: `http://localhost:8100/api/automobiles/:id`

JSON request body :

```
{
  "color": "red",
  "year": 2023,
  "sold": true
}

```

</details>
