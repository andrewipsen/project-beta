import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function Customerlist() {

    const [customers, setCustomers] = useState([]);

    // const handleDelete = async (c) => {
    //     const customersUrl = `http://localhost:8090/api/customers/${c.id}`
    //     const fetchConfig = {
    //         method: "delete"
    //     }
    //     const response = await fetch(customersUrl, fetchConfig)
    //     if(response.ok) {
    //         fetchCustomers();
    //     }
    // }

    const fetchCustomers = async () => {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url);
        if(response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        }
        }
    useEffect(() => {
        fetchCustomers();
    }, [])
    return (
        <>
        <div className="d-grid gap-2 d-sm-flex mt-2 mb-2">
            <Link to="/customers/create" className="btn btn-primary btn-lg px-4 gap-3">Add a Customer</Link>
            </div>
        <table className="table table-striped">
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Address</th>
                <th>Phone Number</th>
            </tr>
            </thead>
            <tbody>
            {customers.map((c) => {
                return (
                <tr key={c.id}>
                    <td>{ c.first_name }</td>
                    <td>{ c.last_name }</td>
                    <td>{ c.address }</td>
                    <td>{ c.phone_number }</td>
                    {/* <td>
                        <button onClick={() => handleDelete(c)}>Delete</button>
                    </td> */}
                </tr>
                );
            })}
            </tbody>
        </table>
        </>
    )

}


export default Customerlist
