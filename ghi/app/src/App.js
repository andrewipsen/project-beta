import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import VehicleModels from './VehicleModelsList';
import Nav from './Nav';
import Manufacturer from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import Automobile from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import ModelForm from './ModelForm';
import Salespeople from './Salespeople';
import SalespersonForm from './SalespersonForm';
import Customerlist from './CustomerList';
import CustomerForm from './CustomerForm';
import Salelist from './SaleList';
import TechniciansList from './TechnicianList';
import SaleForm from './SaleForm';
import SalespersonHistory from './SalespersonHistory';
import TechnicianForm from './TechnicianForm';
import AppointmentList from './AppointmentList';
import AppointmentForm from './AppointmentForm';
import ServiceHistory from './ServiceHistory';

function App() {
	return (
		<BrowserRouter>
			<Nav />
			<div className="container">
				<Routes>
					<Route index element={<MainPage />} />
					<Route path="automobiles">
						<Route path="" element={<Automobile />} />
						<Route path="create" element={<AutomobileForm />} />
					</Route>
					<Route path="manufacturers">
						<Route path="" element={<Manufacturer />} />
						<Route path="create" element={<ManufacturerForm />} />
					</Route>
					<Route path="models">
						<Route path="" element={<VehicleModels />} />
						<Route path="create" element={<ModelForm />} />
					</Route>
					<Route path="salespeople">
						<Route path="" element={<Salespeople />} />
						<Route path="create" element={<SalespersonForm />} />
						<Route path="history" element={<SalespersonHistory />} />
					</Route>
					<Route path="customers">
						<Route path="" element={<Customerlist />} />
						<Route path="create" element={<CustomerForm />} />
					</Route>
					<Route path="technicians">
						<Route path="" element={<TechniciansList />} />
						<Route path="create" element={<TechnicianForm />} />
					</Route>
					<Route path="sales">
						<Route path="" element={<Salelist />} />
						<Route path="create" element={<SaleForm />} />
					</Route>
					<Route path="appointments">
						<Route path="" element={<AppointmentList />} />
						<Route path="create" element={<AppointmentForm />} />
						<Route path="history" element={<ServiceHistory />} />
					</Route>
				</Routes>
			</div>
		</BrowserRouter>
	);
}

export default App;
