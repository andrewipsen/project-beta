import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function AppointmentList() {
	const [appointment, setAppointment] = useState([]);
	const navigate = useNavigate();

	useEffect(() => {
		const fetchURL = 'http://localhost:8080/api/appointments/';
		fetch(fetchURL)
			.then((response) => response.json())
			.then((data) => {
				setAppointment(data.appointment);
			});
	}, []);

	const handleCancel = async (id) => {
		const cancelUrl = `http://localhost:8080/api/appointments/${id}/cancel/`;
		const fetchConfig = {
			method: 'put',
		};
		const response = await fetch(cancelUrl, fetchConfig);
		if (response.ok) {
			setAppointment(appointment.filter((i) => i.id !== id));
			navigate('/appointments');
		}
	};

	const handleFinish = async (id) => {
		const finsihUrl = `http://localhost:8080/api/appointments/${id}/finish/`;
		const fetchConfig = {
			method: 'put',
		};
		const response = await fetch(finsihUrl, fetchConfig);
		if (response.ok) {
			setAppointment(appointment.filter((i) => i.id !== id));
			navigate('/appointments');
		}
	};
	return (
		<>
			<h1 style={{ margin: '25px 0' }}>Service Appointments</h1>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>Vin</th>
						<th>is VIP?</th>
						<th>Customer</th>
						<th>Date/Time</th>
						<th>Technician</th>
						<th>Reason</th>
					</tr>
				</thead>
				<tbody>
					{appointment.map((i) => {
						if (i.status === 'created') {
							return (
								<tr key={i.id}>
									<td>{i.vin}</td>
									<td>{i.vip ? 'Yes' : 'No'}</td>
									<td>{i.customer}</td>
									<td>
										{new Date(i.date_time).toLocaleString('en-US', {
											month: '2-digit',
											day: '2-digit',
											year: '2-digit',
											hour: 'numeric',
											minute: 'numeric',
											hour12: true,
										})}
									</td>
									<td>
										{i.technician.first_name} {i.technician.last_name}
									</td>
									<td>{i.reason}</td>

									<td>
										<button
											onClick={() => handleCancel(i.id)}
											type="button"
											className="btn btn-outline-danger"
										>
											Cancel
										</button>
										<button
											onClick={() => handleFinish(i.id)}
											type="button"
											className="btn btn-success"
										>
											Finish
										</button>
									</td>
								</tr>
							);
						}
					})}
				</tbody>
			</table>
		</>
	);
}
export default AppointmentList;
