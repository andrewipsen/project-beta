import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function SalespersonForm() {
	const navigate = useNavigate();
	const [fName, setFName] = useState('');
	const [lName, setLName] = useState('');
	const [successAlert, setSuccessAlert] = useState(false);

	function handleFNameChange(e) {
		const value = e.target.value;
		setFName(value);
	}
	function handleLNameChange(e) {
		const value = e.target.value;
		setLName(value);
	}

	const handleSubmit = async (e) => {
		e.preventDefault();
		const data = {
			first_name: fName,
			last_name: lName,
		};

		const salespersonUrl = 'http://localhost:8090/api/salespeople/';
		const fetchConfig = {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'content-Type': 'application.json',
			},
		};
		fetch(salespersonUrl, fetchConfig)
			.then((response) => response.json())
			.then(() => {
				setSuccessAlert(true);
				const alertTimeout = setTimeout(() => {
					setSuccessAlert(false);
				}, 3000);
				const navigateTime = setTimeout(() => {
					navigate('/salespeople');
				}, 1000);
				return () => {
					clearTimeout(alertTimeout);
				};
			});
	};

	return (
		<div>
			{successAlert && (
				<div
					className="alert alert-success"
					role="alert"
					style={{
						position: 'fixed',
						top: '1rem',
						right: '1rem',
						zIndex: 1000,
						color: 'white',
						backgroundColor: 'green',
						borderColor: 'darkgreen',
						border: '1px solid',
						borderRadius: '5px',
						padding: '0.75rem 1.25rem',
					}}
				>
					Salesperson added!
				</div>
			)}
			<div className="row">
				<div className="offset-3 col-6">
					<div className="shadow p-4 mt-4">
						<h1>Create a new salesperson</h1>
						<form onSubmit={handleSubmit} id="create-salesperson-form">
							<div className="form-floating mb-3">
								<input
									onChange={handleFNameChange}
									value={fName}
									placeholder="firstname"
									required
									type="text"
									id="firstname"
									className="form-control"
								/>
								<label htmlFor="firstname">First Name</label>
							</div>
							<div className="form-floating mb-3">
								<input
									onChange={handleLNameChange}
									value={lName}
									placeholder="lastname"
									required
									type="text"
									id="lastname"
									className="form-control"
								/>
								<label htmlFor="lastname">Last Name</label>
							</div>
							<div>
								<p>
									Employee ID will be automatically generated upon submission!
								</p>
							</div>
							<button className="btn btn-primary">Create</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
}

export default SalespersonForm;
