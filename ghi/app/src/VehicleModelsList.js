import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function VehicleModels() {
	const [models, setModels] = useState([]);
	const navigate = useNavigate();

	useEffect(() => {
		const fetchURL = 'http://localhost:8100/api/models/';
		fetch(fetchURL)
			.then((response) => response.json())
			.then((data) => setModels(data.models));
	}, []);

	const handleDelete = async (id) => {
		const vehicleURL = `http://localhost:8100/api/models/${id}/`;
		const fetchConfig = {
			method: 'delete',
		};
		const response = await fetch(vehicleURL, fetchConfig);
		if (response.ok) {
			setModels(models.filter((vehicle) => vehicle.id !== id));
			navigate('/models');
		}
	};

	return (
		<>
			<h1 style={{ margin: '25px 0' }}>Models</h1>
			<a href="/models/create">
				<button className="btn btn-primary">Add Model</button>
			</a>
			{models.length === 0 ? (
				<p>There are no models available, stay tuned!</p>
			) : (
				<table className="table table-striped" style={{ tableLayout: 'fixed' }}>
					<thead>
						<tr style={{ textAlign: 'center' }}>
							<th>Manufacturer</th>
							<th>Model Name</th>
							<th>Picture</th>
						</tr>
					</thead>
					<tbody>
						{models.map((model) => {
							return (
								<tr style={{ textAlign: 'center' }} key={model.id}>
									<td>{model.manufacturer.name}</td>
									<td>{model.name}</td>
									<td>
										<img
											src={model.picture_url}
											alt=""
											width="100%"
											height="100%"
										/>
									</td>
									<td style={{ textAlign: 'center' }}>
										<button
											onClick={() => handleDelete(model.id)}
											type="button"
											className="btn btn-outline-danger"
										>
											Delete
										</button>
									</td>
								</tr>
							);
						})}
					</tbody>
				</table>
			)}
		</>
	);
}

export default VehicleModels;
