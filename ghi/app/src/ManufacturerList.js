import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function Manufacturer() {

    const [manufacturers, setManufacturers] = useState ([]);


    const manufacturerDelete = async (manufacturer) => {
        const manufacturerUrl = `http://localhost:8100/api/manufacturers/${manufacturer.id}`
        const fetchConfig = {
            method: "delete"
        }
        const response = await fetch(manufacturerUrl, fetchConfig);
        if(response.ok) {
            fetchManufacturer();
        }
    }


    const fetchManufacturer = async () => {
        const url = "http://localhost:8100/api/manufacturers/";

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers)
        }
    }

    useEffect (() => {
        fetchManufacturer();
    }, []);



    return (
        <>
        <div className="d-grid gap-2 d-sm-flex mt-2 mb-2">
            <Link to="/manufacturers/create" className="btn btn-primary btn-lg px-4 gap-3">Add a Manufacturer</Link>
            </div>
        <table className="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>id</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            {manufacturers.map((manufacturer) => {
                return (
                <tr key={manufacturer.id}>
                    <td>{ manufacturer.name }</td>
                    <td>{ manufacturer.id }</td>
                    <td>
                        <button onClick={() => manufacturerDelete(manufacturer)}>Delete</button>
                    </td>
                </tr>
                );
            })}
            </tbody>
        </table>
        </>
    )
}
export default Manufacturer;
