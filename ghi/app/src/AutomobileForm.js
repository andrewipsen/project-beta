import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

function AutomobileForm() {
	const navigate = useNavigate();
	const [color, setColor] = useState('');
	const [year, setYear] = useState('');
	const [vin, setVin] = useState('');
	const [model, setModel] = useState('');
	const [models, setModels] = useState([]);
    const [successAlert, setSuccessAlert] = useState(false);

	function handleModelChange(event) {
		const value = event.target.value;
		setModel(value);
	}
	function handleColorChange(event) {
		const value = event.target.value;
		setColor(value);
	}
	function handleVinChange(event) {
		const value = event.target.value;
		setVin(value);
	}
	function handleYearChange(event) {
		const value = event.target.value;
		setYear(value);
	}

	//This is for submitting the form
	const handleSubmit = async (e) => {
		e.preventDefault();
		const data = {
			color: color,
			vin: vin,
			year: year,
			model_id: model,
		};

		const automobileUrl = 'http://localhost:8100/api/automobiles/';
		const fetchConfig = {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json',
			},
		};
		fetch(automobileUrl, fetchConfig)
            .then((response) => response.json())
            .then(() => {
                setSuccessAlert(true);
                const alertTimeout = setTimeout(() => {
                    setSuccessAlert(false);
                }, 3000);
                const navigateTime = setTimeout(() => {
                    navigate('/automobiles');
                }, 1000);
                return () => {
                    clearTimeout(alertTimeout);
                };
            });
	};

	//this is for populating the dropdown menu
	const fetchModels = async () => {
		const modelsUrl = 'http://localhost:8100/api/models/';
		const response = await fetch(modelsUrl);

		if (response.ok) {
			const data = await response.json();
			setModels(data.models);
		}
	};
	useEffect(() => {
		fetchModels();
	}, []);

	return (
        <div>
        {successAlert && (
            <div className="alert alert-success" role="alert" style={{
                    position: 'fixed',
                    top: '1rem',
                    right: '1rem',
                    zIndex: 1000,
                    color: 'white',
                    backgroundColor: 'green',
                    borderColor: 'darkgreen',
                    border: '1px solid',
                    borderRadius: '5px',
                    padding: '0.75rem 1.25rem',
                }}>
                Automobile added!
            </div>
        )}
        <div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Create a new Automobile</h1>
					<form onSubmit={handleSubmit} id="create-automobile-form">
						<div className="form-floating mb-3">
							<input
								onChange={handleColorChange}
								value={color}
								placeholder="color"
								required
								type="text"
								id="color"
								className="form-control"
							/>
							<label htmlFor="color">Color</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleVinChange}
								value={vin}
								placeholder="vin"
								required
								type="text"
								id="vin"
								className="form-control"
								maxLength={17}
								style={{ textTransform: 'uppercase' }}
							/>
							<label htmlFor="vin">Vin (max 17 characters)</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleYearChange}
								value={year}
								placeholder="year"
								required
								type="text"
								id="year"
								className="form-control"
							/>
							<label htmlFor="year">year</label>
						</div>
						<div className="mb-3">
							<select
								onChange={handleModelChange}
								value={model}
								required
								className="form-select"
								id="model"
							>
								<option value="">Choose a Model</option>
								{models.map((model) => {
									return (
										<option key={model.id} value={model.id}>
											{model.name}
										</option>
									);
								})}
							</select>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
		</div>
	);
}

export default AutomobileForm;
