import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function CustomerForm() {
	const navigate = useNavigate();
	const [fName, setFName] = useState('');
	const [lName, setLName] = useState('');
	const [address, setAddress] = useState('');
	const [phoneNumber, setPhoneNumber] = useState('');
	const [successAlert, setSuccessAlert] = useState(false);

	function handleFNameChange(e) {
		const value = e.target.value;
		setFName(value);
	}
	function handleLNameChange(e) {
		const value = e.target.value;
		setLName(value);
	}
	function handleAddressChange(e) {
		const value = e.target.value;
		setAddress(value);
	}
	function handlePhoneNumberChange(e) {
		const value = e.target.value;
		setPhoneNumber(value);
	}

	const handleSubmit = async (e) => {
		e.preventDefault();
		const data = {
			first_name: fName,
			last_name: lName,
			address: address,
			phone_number: phoneNumber,
		};

		const customerUrl = 'http://localhost:8090/api/customers/';
		const fetchConfig = {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'content-Type': 'application.json',
			},
		};
		fetch(customerUrl, fetchConfig)
			.then((response) => response.json())
			.then(() => {
				setSuccessAlert(true);
				const alertTimeout = setTimeout(() => {
					setSuccessAlert(false);
				}, 3000);
				const navigateTime = setTimeout(() => {
					navigate('/customers');
				}, 1000);
				return () => {
					clearTimeout(alertTimeout);
				};
			});
	};

	return (
		<div>
			{successAlert && (
				<div
					className="alert alert-success"
					role="alert"
					style={{
						position: 'fixed',
						top: '1rem',
						right: '1rem',
						zIndex: 1000,
						color: 'white',
						backgroundColor: 'green',
						borderColor: 'darkgreen',
						border: '1px solid',
						borderRadius: '5px',
						padding: '0.75rem 1.25rem',
					}}
				>
					Customer added!
				</div>
			)}
			<div className="row">
				<div className="offset-3 col-6">
					<div className="shadow p-4 mt-4">
						<h1>Create a new customer</h1>
						<form onSubmit={handleSubmit} id="create-customer-form">
							<div className="form-floating mb-3">
								<input
									onChange={handleFNameChange}
									value={fName}
									placeholder="firstname"
									required
									type="text"
									id="firstname"
									className="form-control"
								/>
								<label htmlFor="firstname">First Name</label>
							</div>
							<div className="form-floating mb-3">
								<input
									onChange={handleLNameChange}
									value={lName}
									placeholder="lastname"
									required
									type="text"
									id="lastname"
									className="form-control"
								/>
								<label htmlFor="lastname">Last Name</label>
							</div>
							<div className="form-floating mb-3">
								<input
									onChange={handleAddressChange}
									value={address}
									placeholder="address"
									required
									type="text"
									id="address"
									className="form-control"
								/>
								<label htmlFor="address">Address</label>
							</div>
							<div className="form-floating mb-3">
								<input
									onChange={handlePhoneNumberChange}
									value={phoneNumber}
									placeholder="phonenumber"
									required
									type="text"
									id="phonenumber"
									className="form-control"
								/>
								<label htmlFor="phonenumber">Phone Number</label>
							</div>
							<button className="btn btn-primary">Create</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
}

export default CustomerForm;
