from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    import_vin = models.CharField(max_length=200, unique=True, null=True, blank=True)

class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.AutoField(primary_key=True)

    def __str__(self):
        return self.first_name

    def get_api_url(self):
        return reverse("salesperson", kwargs={"pk": self.pk})


class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=12)

    def __str__(self):
        return self.first_name

    def get_api_url(self):
        return reverse("customer", kwargs={"pk": self.pk})


class Sale(models.Model):
    price = models.IntegerField()

    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sale",
        on_delete=models.CASCADE,

    )
    customer = models.ForeignKey(
        Customer,
        related_name="sale",
        on_delete=models.CASCADE,
    )

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sale",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.salesperson.first_name

    def get_api_url(self):
        return reverse("customer", kwargs={"pk": self.pk})
